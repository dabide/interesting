# Interesting - an interest calculator

## Getting things running
This application works both with and without Visual Studio; however, the steps for getting up and running are different for these two scenarios. Please follow the correct set of instructions based on whether or not you are using Visual Studio.

First, there are a set of prerequisites you will need whether or not you are using Visual Studio. Make sure to install these prerequisites before proceeding.

- [NodeJS](http://nodejs.org/) >=4.0.0 This provides the platform on which the build tooling runs. This may be downloaded and installed from the NodeJS website.
- NPM >=3.0.0 This is installed with NodeJS, but if you install a 4.x version of NodeJS, you may have to update this.
- jspm and gulp. These two tools may be installed by running `npm install -g jspm gulp` from a command line after installing NodeJS and NPM
  > **Note:** Sometimes jspm queries GitHub to install packages, but GitHub has a rate limit on anonymous API requests. If you receive a rate limit error, you need to configure jspm with your GitHub credentials. You can do this by executing `jspm registry config github` and following the prompts. If you choose to authorize jspm by an access token instead of giving your password (see GitHub `Settings > Personal Access Tokens`), `public_repo` access for the token is required.

### Running the App in Visual Studio 2017

Additional Prerequisites:
- Visual Studio 2017 RC
- [ASP.NET Core](https://www.microsoft.com/net/core#windows) must be selected during installation.

1. Open the solution file `Interesting.sln`. This file is located up two directories from the file. Visual Studio will automatically run `npm install` and `jspm install` when you first open the solution. You may rerun these commands at any time by running `npm install` from the same directory this readme file is in.
2. Open `Task Runner Explorer`.
3. The project is ready to be run when `Task Runner Explorer` shows that it is running the `gulp watch` command.
4. Open the Package Manager Console, and run `Update-Database`.
5. Press `F5` or click the `Run` button in Visual Studio. Visual Studio will launch your browser for you and navigate to the correct URL.

### Running The Unit Tests

To run the unit tests, first ensure that you have followed the steps above in order to install all dependencies and successfully build the library. Once you have done that, proceed with these additional steps:

1. Ensure that the [Karma](http://karma-runner.github.io/) CLI is installed. If you need to install it, use the following command:

  ```shell
  npm install -g karma-cli
  ```
2. Install Aurelia libs for test visibility:

```shell
jspm install aurelia-framework
jspm install aurelia-http-client
jspm install aurelia-router
```
3. You can now run the tests with this command:

  ```shell
  karma start
  ```

### Running The E2E Tests
Integration tests are performed with [Protractor](http://angular.github.io/protractor/#/).

1. Place your E2E-Tests into the folder ```test/e2e/src```
2. Install the necessary webdriver

  ```shell
  gulp webdriver-update
  ```

3. Configure the path to the webdriver by opening the file ```protractor.conf.js``` and adjusting the ```seleniumServerJar``` property. Typically its only needed to adjust the version number.

4. Make sure your app is built, runs, and is accessible

  ```shell
  gulp build
  dotnet run
  ```

5. In another console run the E2E-Tests

  ```shell
  gulp e2e
  ```

### Exporting bundled production version
A gulp task is already configured for that. Use the following command to export the app:

  ```shell
    gulp export
  ```
The app will be exported into ```export``` directory preserving the directory structure.
#### Configuration
The configuration is done by ```bundles.js``` file.
In addition, ```export.js``` file is available for including individual files.
