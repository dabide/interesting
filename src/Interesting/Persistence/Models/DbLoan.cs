﻿using System;
using System.ComponentModel.DataAnnotations;
using Interesting.Features.Mortgage;

namespace Interesting.Persistence.Models
{
    public class DbLoan
    {
        [Key]
        public int LoanId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public decimal InterestRatePercent { get; set; }
        public int DurationInYears { get; set; }
        public PaymentInterval PaymentInterval { get; set; }
    }
}