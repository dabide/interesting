﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Interesting.Persistence.Models;
using Interesting.Features.Mortgage;

namespace Interesting.Migrations
{
    [DbContext(typeof(LoanContext))]
    [Migration("20170223070834_MyFirstMigration")]
    partial class MyFirstMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Interesting.Persistence.Models.DbLoan", b =>
                {
                    b.Property<int>("LoanId")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Amount");

                    b.Property<int>("DurationInYears");

                    b.Property<decimal>("InterestRatePercent");

                    b.Property<string>("Name");

                    b.Property<int>("PaymentInterval");

                    b.Property<DateTime>("StartDate");

                    b.HasKey("LoanId");

                    b.ToTable("Loans");
                });
        }
    }
}
