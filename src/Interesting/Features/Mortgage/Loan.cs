﻿using System;

namespace Interesting.Features.Mortgage
{
    public class Loan
    {
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public decimal InterestRatePercent { get; set; }
        public int DurationInYears { get; set; }
        public PaymentInterval PaymentInterval { get; set; }
    }
}