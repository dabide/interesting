using System;

namespace Interesting.Features.Mortgage
{
    public class Instalment
    {
        public int InstalmentNumber { get; set; }
        public decimal Interest { get; set; }
        public decimal Downpayment { get; set; }
        public decimal Total { get; set; }
        public decimal AmountLeft { get; set; }
        public DateTime DueDate { get; set; }
    }
}