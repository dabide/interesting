﻿namespace Interesting.Features.Mortgage
{
    public enum PaymentInterval
    {
        Monthly,
        Yearly
    }
}