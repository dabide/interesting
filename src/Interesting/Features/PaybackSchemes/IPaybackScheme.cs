﻿using System.Collections.Generic;
using Interesting.Features.Mortgage;

namespace Interesting.Features.PaybackSchemes
{
    public interface IPaybackScheme
    {
        string Id { get; }
        string Name { get; }
        string Description { get; }
        int RoundingDecimals { get; set; }
        List<Instalment> GetInstalments(Loan loan);
    }
}