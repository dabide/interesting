﻿using System;
using System.Collections.Generic;
using Interesting.Features.Mortgage;

namespace Interesting.Features.PaybackSchemes
{
    public class AnnuityLoan : AbstractPaybackScheme
    {
        public override string Id => "ANNUITY";
        public override string Name => "Annuity Loan";

        public override string Description =>
            "An annuity loan, i.e. the instalment amounts decrease through the duration of the loan";

        public override List<Instalment> GetInstalments(Loan loan)
        {
            throw new NotImplementedException();
        }
    }
}