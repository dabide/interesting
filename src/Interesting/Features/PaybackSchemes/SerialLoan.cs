﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interesting.Features.PaybackSchemes;
using Interesting.Features.Mortgage;
using MoreLinq;

namespace Interesting.Features.PaybackSchemes
{
    public class SerialLoan : AbstractPaybackScheme
    {
        public override string Id => "SERIAL";
        public override string Name => "Serial Loan";

        public override string Description =>
            "A serial loan, i.e. the same amount is paid each month during the whole duration of the loan";

        public override List<Instalment> GetInstalments(Loan loan)
        {
            if (loan == null) throw new ArgumentNullException(nameof(loan));

            var numberOfInstalments = GetNumberOfInstalments(loan);
            var numberOfInstalmentsPerYear = GetYearlyNumberOfInstalments(loan);

            var monthlyDownPayment = Math.Round(loan.Amount / numberOfInstalments, RoundingDecimals);

            return MoreEnumerable.GenerateByIndex(i =>
                {
                    var amountLeftBefore = loan.Amount - i * monthlyDownPayment;
                    var interests = Math.Round(
                        amountLeftBefore * loan.InterestRatePercent / 100 / numberOfInstalmentsPerYear,
                        RoundingDecimals);
                    var instalmentNumber = i + 1;
                    var lastInstalment = instalmentNumber == numberOfInstalments;

                    var downpayment = lastInstalment ? amountLeftBefore : monthlyDownPayment;
                    var total = interests + downpayment;

                    var instalment = new Instalment
                    {
                        InstalmentNumber = instalmentNumber,
                        Downpayment = downpayment,
                        Interest = interests,
                        AmountLeft = amountLeftBefore - monthlyDownPayment,
                        Total = total,
                        DueDate = GetDueDate(loan, instalmentNumber)
                    };
                    return instalment;
                })
                .Take(numberOfInstalments)
                .ToList();
        }
    }
}