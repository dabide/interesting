using System;
using System.Collections.Generic;
using Interesting.Features.Mortgage;

namespace Interesting.Features.PaybackSchemes
{
    public abstract class AbstractPaybackScheme : IPaybackScheme
    {
        public abstract string Id { get; }
        public abstract string Name { get; }
        public abstract string Description { get; }
        public virtual int RoundingDecimals { get; set; } = 2;
        public abstract List<Instalment> GetInstalments(Loan loan);

        protected int GetNumberOfInstalments(Loan loan)
        {
            switch (loan.PaymentInterval)
            {
                case PaymentInterval.Monthly:
                    return loan.DurationInYears * 12;
                case PaymentInterval.Yearly:
                    return loan.DurationInYears;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected int GetYearlyNumberOfInstalments(Loan loan)
        {
            switch (loan.PaymentInterval)
            {
                case PaymentInterval.Monthly:
                    return 12;
                case PaymentInterval.Yearly:
                    return 1;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected DateTime GetDueDate(Loan loan, int instalmentNumber)
        {
            switch (loan.PaymentInterval)
            {
                case PaymentInterval.Monthly:
                    return loan.StartDate.AddMonths(instalmentNumber);
                case PaymentInterval.Yearly:
                    return loan.StartDate.AddYears(instalmentNumber);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}