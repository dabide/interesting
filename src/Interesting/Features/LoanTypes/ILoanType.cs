﻿namespace Interesting.Features.LoanTypes
{
    public interface ILoanType
    {
        string Id { get; }
        string Name { get; }
        string Icon { get; }
        string Description { get; }
    }

    public class HousingLoan : ILoanType
    {
        public string Id => "HOUSING_LOAN";
        public string Name => "Housing loan";
        public string Icon => "home";
        public string Description => "A loan to buy a house";
    }
}
