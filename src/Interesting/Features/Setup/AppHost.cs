using Funq;
using ServiceStack;
using ServiceStack.Text;

namespace Interesting.Features.Setup
{
    public class AppHost : AppHostBase
    {
        public AppHost() : base("Interesting API", typeof(AppHost).GetAssembly())
        {
            JsConfig.DateHandler = DateHandler.ISO8601;
        }

        public override void Configure(Container container)
        {
        }
    }
}