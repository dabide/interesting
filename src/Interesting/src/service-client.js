import { inject, LogManager } from 'aurelia-framework';
import { Notification } from 'aurelia-notification';
import { HttpClient, json } from 'aurelia-fetch-client';
import "humane-js/themes/libnotify.css!";

const logger = LogManager.getLogger('service-client');

@inject(Notification, HttpClient)
export class ServiceClient {
    constructor(notification, httpClient) {
        this.notification = notification;
        
        httpClient.configure(config => {
            config
                .rejectErrorResponses()
                .withBaseUrl('api/')
                .withDefaults({
                    credentials: 'same-origin',
                    headers: {
                        'Accept': 'application/json',
                        'X-Requested-With': 'Fetch'
                    }
                })
                .withInterceptor({
                    request(request) {
                        logger.debug(`Requesting ${request.method} ${request.url}`);
                        return request;
                    },
                    response(response) {
                        logger.debug(`Received ${response.status} ${response.url}`);
                        return response;
                    }
                });
        });

        this.httpClient = httpClient;
    }

    get(query) {
        return this.httpClient.fetch(query)
        .then(response =>{
            logger.debug('response', response);
            return response.json();
        })
        .catch(error => {
            logger.debug('GET error', error);
            this.notification.error('An error occurred');
        });
    }

    post(query, data) {
        return this.httpClient.fetch(query, {
            method: 'post',
            body: json(data)
        })
        .then(response => response.json())
        .catch(error => {
            logger.debug('POST error', error);
            this.notification.error('An error occurred');
        });
    }
}