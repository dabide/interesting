import { inject, LogManager } from 'aurelia-framework';
import { ServiceClient } from 'service-client';

const logger = LogManager.getLogger('loan-calculation-service');

@inject(ServiceClient)
export class LoanCalculationService {
    constructor(serviceClient) {
        this.serviceClient = serviceClient;
    }

    calculate(loan) {
        logger.debug('loan', loan);
        return this.serviceClient.post('loan-calculation', loan);
    }
}
