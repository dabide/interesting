export class App {
  configureRouter(config, router) {
    config.title = 'Interesting';
    config.map([
      { route: ['', 'calculator'], name: 'calculator', moduleId: 'calculator', nav: true, title: 'Calculator' },
      { route: 'loan-types', name: 'loanTypes', moduleId: 'loan-types', nav: true, title: 'Loan Types' }
    ]);

    this.router = router;
  }
}
