import { inject, LogManager } from 'aurelia-framework';
import { ServiceClient } from 'service-client';

@inject(ServiceClient)
export class LoanService {
    constructor(serviceClient) {
        this.serviceClient = serviceClient;
    }

    getLoans() {
        return this.serviceClient.get('loan')
            .then(response => {
                return response.loans;
            });
    }
}
