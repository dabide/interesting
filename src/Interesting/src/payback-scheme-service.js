import { inject, LogManager } from 'aurelia-framework';
import { ServiceClient } from 'service-client';

@inject(ServiceClient)
export class PaybackSchemeService {
    constructor(serviceClient) {
        this.serviceClient = serviceClient;
    }

    getPaybackSchemes() {
        return this.serviceClient.get('payback-scheme')
            .then(response => {
                return response.paybackSchemes;
            });
    }
}
