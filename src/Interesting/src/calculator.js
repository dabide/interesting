import { inject, observable, LogManager } from 'aurelia-framework';
import { LoanTypeService } from 'loan-type-service';
import { LoanService } from 'loan-service';
import { PaybackSchemeService } from 'payback-scheme-service';
import { LoanCalculationService } from 'loan-calculation-service';

const logger = LogManager.getLogger('calculator');

@inject(LoanTypeService, LoanService, PaybackSchemeService, LoanCalculationService)
export class Calculator {
  constructor(loanTypeService, loanService, paybackSchemeService, loanCalculationService) {
    this.loanTypeService = loanTypeService;
    this.loanService = loanService;
    this.paybackSchemeService = paybackSchemeService;
    this.loanCalculationService = loanCalculationService;
  }

  activate() {
    return Promise.all([
      this.getLoanTypes(),
      this.getPaybackSchemes(),
      this.getLoans()
    ]);
  }

  getLoanTypes() {
    return this.loanTypeService.getLoanTypes()
      .then(loanTypes => {
        this.loanTypes = loanTypes;
        this.selectedLoanType = loanTypes[0]; //TODO: Fix this when multiple loan types are available
      });
  }

  getPaybackSchemes() {
    return this.paybackSchemeService.getPaybackSchemes()
      .then(paybackSchemes => {
        this.paybackSchemes = paybackSchemes;
        this.selectedPaybackScheme = paybackSchemes[1]; //TODO: Fix this when payback schemes are fully implemented
      });
  }

  getLoans() {
    return this.loanService.getLoans()
      .then(loans => this.loans = loans);
  }

  setLoan(loan) {
    //TODO: Set loan type and payback scheme
    this.amount = loan.amount;
    this.startDate = loan.startDate;
    this.interestRate = loan.interestRatePercent;
    this.duration = loan.durationInYears;

    this.calculate();
  }

  calculate() {
    return this.loanCalculationService.calculate({
      loanTypeId: this.selectedLoanType.id,
      paybackSchemeId: this.selectedPaybackScheme.id,
      amount: this.amount,
      startDate: new Date(),
      interestRatePercent: this.interestRate,
      durationInYears: this.duration,
      paymentInterval: 'Monthly'
    }).then(data => {
      logger.debug('data', data);
      this.instalments = data.instalments;
    });
  }
}
