import { inject, LogManager } from 'aurelia-framework';
import { ServiceClient } from 'service-client';

@inject(ServiceClient)
export class LoanTypeService {
    constructor(serviceClient) {
        this.serviceClient = serviceClient;
    }

    getLoanTypes() {
        return this.serviceClient.get('loan-type')
            .then(response => {
                return response.loanTypes;
            });
    }
}
