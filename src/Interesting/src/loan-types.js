import {inject, LogManager} from 'aurelia-framework';
import {LoanTypeService} from 'loan-type-service';

const logger = LogManager.getLogger('loan-types');

@inject(LoanTypeService)
export class LoanTypes {
  constructor(loanTypeService) {
    this.loanTypeService = loanTypeService;
  }

  activate() {
    return this.loanTypeService.getLoanTypes()
      .then(loanTypes => this.loanTypes = loanTypes);
  }
}
