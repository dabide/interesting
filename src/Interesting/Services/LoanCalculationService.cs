﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interesting.Features.LoanTypes;
using Interesting.Features.Mortgage;
using Interesting.Features.PaybackSchemes;
using ServiceStack;
using ServiceStack.FluentValidation;

namespace Interesting.Services
{
    public class LoanCalculationService : Service
    {
        private readonly Dictionary<string, ILoanType> _loanTypes;
        private readonly Dictionary<string, IPaybackScheme> _paybackSchemes;

        public LoanCalculationService(IEnumerable<ILoanType> loanTypes, IEnumerable<IPaybackScheme> paybackSchemes)
        {
            _loanTypes = loanTypes.ToDictionary(t => t.Id);
            _paybackSchemes = paybackSchemes.ToDictionary(p => p.Id);
        }

        public LoanCalculationResponse Post(LoanCalculationRequest request)
        {
            if (!_loanTypes.ContainsKey(request.LoanTypeId))
                throw new ArgumentOutOfRangeException(nameof(request.LoanTypeId));
            if (!_paybackSchemes.ContainsKey(request.PaybackSchemeId))
                throw new ArgumentOutOfRangeException(nameof(request.PaybackSchemeId));

            var loanType = _loanTypes[request.LoanTypeId];
            var paybackScheme = _paybackSchemes[request.PaybackSchemeId];
            var loan = request.ConvertTo<Loan>();
            var instalments = paybackScheme.GetInstalments(loan);

            return new LoanCalculationResponse
            {
                Instalments = instalments.Select(i => i.ConvertTo<InstalmentDto>()).ToList()
            };
        }
    }

    [Route("/api/loan-calculation")]
    public class LoanCalculationRequest
    {
        public string LoanTypeId { get; set; }
        public string PaybackSchemeId { get; set; }
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public decimal InterestRatePercent { get; set; }
        public int DurationInYears { get; set; }
        public PaymentInterval PaymentInterval { get; set; }
    }

    public class LoanCalculationResponse
    {
        public List<InstalmentDto> Instalments { get; set; }
    }

    public class LoanCalculationRequestValidator : AbstractValidator<LoanCalculationRequest>
    {
        public LoanCalculationRequestValidator()
        {
            RuleFor(r => r.LoanTypeId).NotEmpty();
            RuleFor(r => r.PaybackSchemeId).NotEmpty();
            RuleFor(r => r.Amount).GreaterThan(0m);
            RuleFor(r => r.InterestRatePercent).GreaterThan(0m);
            RuleFor(r => r.DurationInYears).GreaterThanOrEqualTo(1);
        }
    }

    public class InstalmentDto
    {
        public int InstalmentNumber { get; set; }
        public decimal Interest { get; set; }
        public decimal Downpayment { get; set; }
        public decimal Total { get; set; }
        public decimal AmountLeft { get; set; }
        public DateTime DueDate { get; set; }
    }
}