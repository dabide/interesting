﻿using System.Collections.Generic;
using System.Linq;
using Interesting.Features.LoanTypes;
using ServiceStack;

namespace Interesting.Services
{
    public class LoanTypeService : Service
    {
        private readonly IEnumerable<ILoanType> _loanTypes;

        public LoanTypeService(IEnumerable<ILoanType> loanTypes)
        {
            _loanTypes = loanTypes;
        }

        public LoanTypeResponse Get(LoanTypeRequest loanTypeRequest)
        {
            return new LoanTypeResponse
            {
                LoanTypes = _loanTypes.Select(l => l.ConvertTo<LoanTypeDto>()).ToList()
            };
        }
    }

    [Route("/api/loan-type")]
    public class LoanTypeRequest : IReturn<LoanTypeResponse>
    {
    }

    public class LoanTypeResponse
    {
        public List<LoanTypeDto> LoanTypes { get; set; }
    }

    public class LoanTypeDto
    {
        public string Id { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}