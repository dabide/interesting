﻿using System.Collections.Generic;
using System.Linq;
using Interesting.Features.PaybackSchemes;
using ServiceStack;

namespace Interesting.Services
{
    public class PaybackSchemeService : Service
    {
        private readonly IEnumerable<IPaybackScheme> _paybackSchemes;

        public PaybackSchemeService(IEnumerable<IPaybackScheme> paybackSchemes)
        {
            _paybackSchemes = paybackSchemes;
        }

        public PaybackSchemeResponse Get(PaybackSchemeRequest paybackSchemeRequest)
        {
            return new PaybackSchemeResponse
            {
                PaybackSchemes = _paybackSchemes.Select(s => s.ConvertTo<PaybackSchemeDto>()).ToList()
            };
        }
    }

    [Route("/api/payback-scheme")]
    public class PaybackSchemeRequest : IReturn<PaybackSchemeResponse>
    {
    }

    public class PaybackSchemeResponse
    {
        public List<PaybackSchemeDto> PaybackSchemes { get; set; }
    }

    public class PaybackSchemeDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}