﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interesting.Features.Mortgage;
using Interesting.Persistence.Models;
using ServiceStack;

namespace Interesting.Services
{
    public class LoanService : Service
    {
        private readonly LoanContext _context;

        public LoanService(LoanContext context)
        {
            _context = context;

            CreateDummyData();
        }

        public LoanResponse Get(LoanRequest request)
        {
            return new LoanResponse
            {
                Loans = _context.Loans.Select(l => l.ConvertTo<LoanDto>()).ToList()
            };
        }

        private void CreateDummyData()
        {
            if (!_context.Loans.Any(l => l.Name == "TwentyfiveYears"))
            {
                _context.Loans.Add(new DbLoan
                {
                    Amount = 2500000m,
                    DurationInYears = 25,
                    InterestRatePercent = 2.5m,
                    PaymentInterval = PaymentInterval.Monthly,
                    StartDate = DateTime.Now,
                    Name = "TwentyfiveYears"
                });

                _context.SaveChanges();
            }

            if (!_context.Loans.Any(l => l.Name == "TenIsBetter"))
            {
                _context.Loans.Add(new DbLoan
                {
                    Amount = 2000000m,
                    DurationInYears = 10,
                    InterestRatePercent = 2.0m,
                    PaymentInterval = PaymentInterval.Monthly,
                    StartDate = DateTime.Now,
                    Name = "TenIsBetter"
                });

                _context.SaveChanges();
            }
        }
    }

    [Route("/api/loan")]
    public class LoanRequest : IReturn<LoanResponse>
    {
    }

    public class LoanResponse
    {
        public List<LoanDto> Loans { get; set; }
    }

    public class LoanDto
    {
        public int LoanId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public decimal InterestRatePercent { get; set; }
        public int DurationInYears { get; set; }
        public PaymentInterval PaymentInterval { get; set; }
    }
}