using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Interesting.Persistence.Models;

namespace Interesting.Controllers
{
    [Produces("application/json")]
    [Route("api/DbLoans")]
    public class DbLoansController : Controller
    {
        private readonly LoanContext _context;

        public DbLoansController(LoanContext context)
        {
            _context = context;
        }

        // GET: api/DbLoans
        [HttpGet]
        public IEnumerable<DbLoan> GetLoans()
        {
            return _context.Loans;
        }

        // GET: api/DbLoans/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDbLoan([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dbLoan = await _context.Loans.SingleOrDefaultAsync(m => m.LoanId == id);

            if (dbLoan == null)
            {
                return NotFound();
            }

            return Ok(dbLoan);
        }

        // PUT: api/DbLoans/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDbLoan([FromRoute] int id, [FromBody] DbLoan dbLoan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dbLoan.LoanId)
            {
                return BadRequest();
            }

            _context.Entry(dbLoan).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DbLoanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DbLoans
        [HttpPost]
        public async Task<IActionResult> PostDbLoan([FromBody] DbLoan dbLoan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Loans.Add(dbLoan);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DbLoanExists(dbLoan.LoanId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetDbLoan", new { id = dbLoan.LoanId }, dbLoan);
        }

        // DELETE: api/DbLoans/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDbLoan([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dbLoan = await _context.Loans.SingleOrDefaultAsync(m => m.LoanId == id);
            if (dbLoan == null)
            {
                return NotFound();
            }

            _context.Loans.Remove(dbLoan);
            await _context.SaveChangesAsync();

            return Ok(dbLoan);
        }

        private bool DbLoanExists(int id)
        {
            return _context.Loans.Any(e => e.LoanId == id);
        }
    }
}