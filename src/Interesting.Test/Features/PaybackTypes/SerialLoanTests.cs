﻿using System;
using System.Linq;
using FluentAssertions;
using Interesting.Features.Mortgage;
using Interesting.Features.PaybackSchemes;
using Xunit;

namespace Interesting.Test.Features.PaybackTypes
{
    public class SerialLoanTests : TestBase
    {
        public SerialLoanTests()
        {
            _serialLoan = new SerialLoan { RoundingDecimals = 2 };
        }

        private readonly IPaybackScheme _serialLoan;

        [Theory]
        [InlineData(2, PaymentInterval.Monthly, 24)]
        [InlineData(5, PaymentInterval.Monthly, 60)]
        [InlineData(2, PaymentInterval.Yearly, 2)]
        [InlineData(5, PaymentInterval.Yearly, 5)]
        public void GetInstalments_should_return_correct_number_of_instalments(int years,
            PaymentInterval paymentInterval, int expectedInstalments)
        {
            var loan = new Loan
            {
                DurationInYears = years,
                InterestRatePercent = 3.5m,
                PaymentInterval = paymentInterval,
                StartDate = DateTime.Now
            };
            var instalments = _serialLoan.GetInstalments(loan);
            instalments.Count.Should().Be(expectedInstalments);
        }

        [Theory]
        [InlineData(10, 3.5, 1000000, 11250, 8358.04)]
        [InlineData(25, 5, 1000000, 7500, 3348.22)]
        public void GetInstalments_should_return_correct_monthly_instalments(int years, decimal interest,
            decimal amount, decimal expectedFirstMonth, decimal expectedLastMonth)
        {
            var loan = new Loan
            {
                Amount = amount,
                DurationInYears = years,
                InterestRatePercent = interest,
                PaymentInterval = PaymentInterval.Monthly,
                StartDate = DateTime.Now
            };
            var instalments = _serialLoan.GetInstalments(loan);

            instalments.First().Total.Should().Be(expectedFirstMonth);
            instalments.Last().Total.Should().Be(expectedLastMonth);
        }

        [Fact]
        public void Description_should_not_be_empty()
        {
            _serialLoan.Description.Should().NotBeEmpty();
        }

        [Fact]
        public void GetInstalments_with_invalid_parameters_should_throw()
        {
            Calling(() => _serialLoan.GetInstalments(null))
                .ShouldThrow<ArgumentNullException>()
                .Where(a => a.ParamName == "loan");
        }

        [Fact]
        public void Name_should_not_be_empty()
        {
            _serialLoan.Name.Should().NotBeEmpty();
        }
    }
}